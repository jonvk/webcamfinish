CC	= g++
CFLAGS = -Wall -pedantic `gphoto2-config --cflags`
LDFLAGS = `gphoto2-config --libs` -lopencv_core -lopencv_imgproc -lopencv_highgui -lrt
DEBUG = -g

all: photo

debug: photo
	$(CC) $(CFLAGS) photo.cpp $(LDFLAGS) $(DEBUG) -o photo
photo: photo.cpp
	$(CC) $(CFLAGS) photo.cpp $(LDFLAGS) -o photo


clean:
	rm -f photo photo.o