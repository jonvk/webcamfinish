#include <iostream>

#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <stdio.h>

#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "gphoto2/gphoto2.h"

#define STEPS 360
#define YPIX 400

using namespace std;

double compare(cv::Mat row1, cv::Mat row2, cv::Mat ones, double targetavg);

bool shoot(Camera *camera, GPContext *context);
  
void get_filename(char * filename);

bool get_camera(Camera *camera, GPContext *camera_context);

int main() {

  GPContext * camera_context = NULL;
  Camera* camera = 0;
  
  get_camera(camera, camera_context);

  cv::VideoCapture cap(0);
  if (!cap.isOpened())
    return -1;
  
  cap.set(CV_CAP_PROP_FRAME_HEIGHT, 480);
  cap.set(CV_CAP_PROP_BRIGHTNESS, 0.5);
  
  cv::Mat frame;
  cv::Mat image(cv::Size(640, STEPS), CV_8UC3);
  cv::Mat row(cv::Size(640, 1), CV_8UC3);
  cv::Mat tmprow(cv::Size(640, 1), CV_8UC3);
  cv::Mat oldrow(cv::Size(640, 1), CV_8UC3);
  cv::Mat edge(cv::Size(320,1), CV_8UC3);
  cv::Mat ones(cv::Size(640, 1), CV_8UC3);
  char outname[60];
  double diff, targetavg;

  int trigger;

  ones = cv::Scalar(1,1,1);

  // for (int i=3; i>=0; i--) {
  //   cout << i << endl;
  //   sleep(1);
  // }
  
  // Give camera a chance to auto configure exposure, etc.
  for (int i=0; i<30; i++) {
    cap >> frame;
  }

  // Initialize
  cap >> frame;
  cv::GaussianBlur(frame.row(YPIX), oldrow, cv::Size(5, 1), 2, 0);

  trigger = 0;
  targetavg = oldrow.dot(ones);

  for (int i=0; i<STEPS; i++) {
    row = image.row(i);
    // Load a middle row of picture to array with a Gaussian blur to reduce noise
    cv::GaussianBlur(frame.row(YPIX), tmprow, cv::Size(5, 1), 2, 0);
    // Populate image
    frame.row(YPIX).copyTo(row);

    diff = compare(oldrow, tmprow, ones, targetavg);

    if (diff > 15000) {
      if (trigger == 0) {
	cout << "Edge" << endl;
	get_filename(outname);
	cv::imwrite(outname, frame);
	//	shoot(camera, camera_context);
	edge = row(cv::Range(0,1), cv::Range(0,320));
	edge = edge + cv::Scalar(77,188,77);
	trigger = 3;
      }
    }
    else if (trigger > 0 && diff < 10000) {
      trigger--;
    }

    // Keep row to compare
    tmprow.copyTo(oldrow);
    // Capture a new image
    cap >> frame;
  }
  cout << endl;

  cv::imwrite("out.jpg", image);

  return 0;
}


/**
 * Compare two rows and return the sum diff^2.
 **/
double compare(cv::Mat row1, cv::Mat row2, cv::Mat ones, double targetavg) {
  cv::Mat oldshortrow(cv::Size(600, 1), CV_8UC3);
  cv::Mat shortrow(cv::Size(600, 1), CV_8UC3);
  cv::Mat comprow(cv::Size(600, 1), CV_8UC3);
  double diff, mindiff, avg, ratio;
  // Compare the new and next row. Allow a +/- 20 pixel shift
  // Usefull to compensate for camera vibration.
  oldshortrow = row1(cv::Range(0,1), cv::Range(20,620));
  mindiff = -1;
  avg = row2.dot(ones);
  ratio = (targetavg / avg);
  row2 *= ratio;
  for (int i=0; i<40; i++) {
    shortrow=row2(cv::Range(0,1), cv::Range(i,600+i));
    comprow = shortrow-oldshortrow;
    diff = comprow.dot(comprow);
    if (mindiff < 0 || diff < mindiff) {
      mindiff = diff;
    }
  }
  return mindiff;
}


/**
 * Take a picture!
 * Pass it an initialized camera and context.
 **/
bool shoot(Camera *camera, GPContext *context) {
  int ret;
  CameraFilePath path;
  ret = gp_camera_capture(camera, GP_CAPTURE_IMAGE, &path, context);

  if (ret >= 0) {
    std::cout  << ret << " " << path.folder << " " << path.name  << std::endl;

    CameraFile *cfile;
    gp_file_new(&cfile);
    gp_camera_file_get(camera, path.folder, path.name, GP_FILE_TYPE_NORMAL, cfile, context);
    gp_file_save( cfile, path.name );
    gp_file_unref( cfile );
  }

  return ret >= GP_OK;
}

/**
 * Provide a filename for output.
 **/
void get_filename(char * filename) {
  struct timespec ntime;
  int nano;
  char tmp[26] = {};
  time_t current;
  struct tm * timeptr;

  current = time(NULL);  
  timeptr = localtime(&current);
  clock_gettime(CLOCK_REALTIME, &ntime);
  
  nano = (int) ntime.tv_nsec / 1000000;

  strftime(tmp, 25, "%F-%H:%M:%S", timeptr);
  sprintf(filename, "finish-%s-%02d.jpg", tmp, nano);
}

bool get_camera(Camera * camera, GPContext * camera_context) {
  camera_context = gp_context_new();
  if (!camera_context)
    return 1;


  int ret;
  GPPortInfoList *portinfolist = NULL;
  CameraAbilitiesList *abilities = NULL;
  CameraAbilitiesList *capture_ability = NULL;
  CameraList* all_camera_list = NULL;
  CameraList* camera_list = NULL;
  char camera_model[128];
  char camera_port[128];
  CameraAbilities	camera_abilities;

  camera_model[0] = 0;
  camera_port[0] = 0;
  /* Load all the port drivers we have... */
  ret = gp_port_info_list_new(&portinfolist);
  if (ret < GP_OK) return ret;

  ret = gp_port_info_list_load(portinfolist);
  if (ret < 0) return ret;
	
  gp_abilities_list_new(&abilities);
  gp_abilities_list_new(&capture_ability);
  gp_abilities_list_load(abilities, camera_context);
  ret = gp_abilities_list_count(abilities);
  for (int i=0; i<ret; i++) {
    gp_abilities_list_get_abilities(abilities, i, &camera_abilities);
    if ((camera_abilities.operations & GP_OPERATION_CAPTURE_IMAGE) == GP_OPERATION_CAPTURE_IMAGE) {
      gp_abilities_list_append(capture_ability, camera_abilities);
    }
  }
  gp_abilities_list_free(abilities);
  
  gp_list_new(&all_camera_list);
  
  ret = gp_abilities_list_detect(capture_ability, portinfolist, all_camera_list, camera_context);
  
  ret = gp_list_count(all_camera_list);
  
  if (ret == 0) {
    cout << "No cameras" << endl;
    return -1;
  }
  cout << "cameras: " << ret << endl;
  
  const char *name;
  gp_list_get_name(camera_list, 0, &name);
  
  gp_camera_new(&camera);
  
  ret = gp_abilities_list_lookup_model(abilities, camera_model);
  ret = gp_abilities_list_get_abilities(abilities, ret, &camera_abilities);
  gp_camera_set_abilities(camera, camera_abilities);
  ret = gp_port_info_list_lookup_path(portinfolist, camera_port);
  return true;
}
